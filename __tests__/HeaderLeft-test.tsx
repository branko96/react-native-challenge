/**
 * @format
 */

import 'react-native';
import React from 'react';

// Note: test renderer must be required after react-native.
import renderer from 'react-test-renderer';
import { HeaderLeft } from '../components/common';

const createTestProps = (props: Object) => ({
  navigation: {
    getState: jest
      .fn()
      .mockReturnValueOnce({ routes: [{ key: 'Home' }], index: 0 }),
  },
  ...props,
});

it('renders correctly', () => {
  const props = createTestProps({});
  renderer.create(<HeaderLeft navigation={props.navigation} />);
});
