/**
 * @format
 */

import 'react-native';
import React from 'react';

// Note: test renderer must be required after react-native.
import renderer from 'react-test-renderer';
import { Button } from '../components/common';

it('renders correctly', () => {
  const onPress = jest.fn();
  renderer.create(<Button title="Button" onPress={onPress} />);
});
