/**
 * @format
 */

import 'react-native';
import React from 'react';

// Note: test renderer must be required after react-native.
import renderer from 'react-test-renderer';
import { LoginForm } from '../components/auth';

it('renders correctly', () => {
  const onSubmit = jest.fn();
  const initialValues = {
    username: '',
    password: '',
  };
  renderer.create(
    <LoginForm onSubmit={onSubmit} initialValues={initialValues} />,
  );
});
