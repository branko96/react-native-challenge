/**
 * @format
 */

import 'react-native';
import React from 'react';

// Note: test renderer must be required after react-native.
import renderer from 'react-test-renderer';
import { ItemCard } from '../components/ui';
import { itemMock } from '../__mocks__/itemMock';

it('renders correctly', () => {
  const onPress = jest.fn();
  renderer.create(<ItemCard item={itemMock} onPress={onPress} />);
});
