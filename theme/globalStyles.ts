import { TextStyle } from 'react-native';

const colors = {
  borderGrey: '#F2F5FC',
  black: '#0A132C',
  turquoise: '#019FB5',
  lightBlue: '#1FC4DB',
  gray: '#6B7280',
  white: '#fff',
  badgeRed: '#FDDCDC',
  badgeGreen: '#D1FAE5',
  red: '#A50606',
  green: '#065F46',
};

const alerts = {
  errorAlert: {
    backgroundColor: colors.badgeRed,
    padding: 16,
    marginHorizontal: 16,
    marginTop: 20,
    borderRadius: 8,
  },
  errorAlertTitle: {
    color: colors.red,
    textAlign: 'center',
    fontSize: 16,
    fontWeight: 'normal',
  } as TextStyle,
};

const backgrounds = {
  greyBackground: '#F8F8FA',
};

export const theme = {
  colors,
  alerts,
  backgrounds,
};
