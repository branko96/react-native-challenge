import axios from 'axios';

export const getAssets = async () => {
  return axios
    .get('https://api.coincap.io/v2/assets?limit=5')
    .then(response => {
      return response.data.data;
    })
    .catch(error => {
      console.log(error);
    });
};

export const getAssetById = async (id: string) => {
  return axios
    .get(`https://api.coincap.io/v2/assets/${id}`)
    .then(response => {
      console.log(response);
      return response.data.data;
    })
    .catch(error => {
      console.log(error);
    });
};
