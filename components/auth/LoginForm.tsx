import React from 'react';
import { StyleSheet, TextInput, Text, View } from 'react-native';
import { Formik } from 'formik';
import { theme } from '../../theme/globalStyles';
import Button from '../common/Button';

export interface LoginFormValues {
  username: string;
  password: string;
}

interface LoginFormProps {
  onSubmit: (values: any) => void;
  initialValues: LoginFormValues;
}

const LoginForm = ({ initialValues, onSubmit }: LoginFormProps) => (
  <Formik
    initialValues={initialValues}
    validate={values => {
      const errors: Partial<LoginFormValues> = {};
      if (!values.username) {
        errors.username = 'Required';
      }
      if (!values.password) {
        errors.password = 'Required';
      }
      return errors;
    }}
    onSubmit={values => {
      onSubmit(values);
    }}>
    {({ handleChange, errors, touched, handleBlur, handleSubmit, values }) => (
      <>
        <View style={styles.inputsContainer}>
          <TextInput
            style={[
              styles.input,
              touched.username && errors.username ? styles.inputError : {},
            ]}
            placeholder="Enter your name"
            onChangeText={handleChange('username')}
            onBlur={handleBlur('username')}
            value={values.username}
            placeholderTextColor={theme.colors.gray}
          />
          {errors.username && touched.username && (
            <Text style={styles.error}>{errors.username}</Text>
          )}
          <TextInput
            style={[
              styles.input,
              touched.password && errors.password ? styles.inputError : {},
            ]}
            secureTextEntry
            onChangeText={handleChange('password')}
            onBlur={handleBlur('password')}
            value={values.password}
            placeholder="Enter your password"
            placeholderTextColor={theme.colors.gray}
          />
          {errors.password && touched.password && (
            <Text style={styles.error}>{errors.password}</Text>
          )}
        </View>
        <View style={styles.buttonContainer}>
          <Button
            title="Sign in"
            onPress={handleSubmit}
            style={styles.button}
          />
        </View>
      </>
    )}
  </Formik>
);

const styles = StyleSheet.create({
  inputsContainer: {
    width: '100%',
    paddingHorizontal: 30,
  },
  buttonContainer: {
    width: '100%',
    paddingHorizontal: 34,
  },
  button: {
    marginTop: 30,
  },
  input: {
    height: 40,
    width: '100%',
    marginBottom: 12,
    borderWidth: 1,
    paddingHorizontal: 10,
    borderColor: '#D1D5DB',
    borderRadius: 6,
    color: theme.colors.gray,
  },
  inputError: {
    borderColor: theme.colors.red,
  },
  error: {
    color: theme.colors.red,
    fontSize: 12,
    marginBottom: 10,
  },
});

export default LoginForm;
