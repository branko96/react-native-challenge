import React from 'react';
import { StyleSheet, TouchableOpacity, Text, ViewStyle } from 'react-native';
import { theme } from '../../theme/globalStyles';

interface ButtonProps {
  title: string;
  onPress: () => void;
  style?: ViewStyle;
}

function Button({ title, onPress, style }: ButtonProps) {
  return (
    <TouchableOpacity onPress={onPress} style={[styles.button, style]}>
      <Text style={styles.title}>{title}</Text>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  button: {
    backgroundColor: theme.colors.lightBlue,
    padding: 10,
    width: '100%',
    borderRadius: 6,
  },
  title: {
    color: theme.colors.white,
    textAlign: 'center',
    fontSize: 16,
  },
});

export default Button;
