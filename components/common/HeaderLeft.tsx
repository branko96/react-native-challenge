import * as React from 'react';
import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { theme } from '../../theme/globalStyles';

const backArrowImage = require('../../assets/chevron-left.png');

interface HeaderLeftProps {
  navigation: any;
}

const HeaderLeft = ({ navigation }: HeaderLeftProps) => {
  const state = navigation.getState();
  const { routes, index: stateIndex } = state;

  return (
    <View>
      <TouchableOpacity
        onPress={() => {
          navigation.goBack();
        }}
        style={styles.goBackButton}>
        <Image style={styles.backArrowImage} source={backArrowImage} />
        {routes.length > 1 && (
          <Text style={styles.previousScreenName}>
            {routes[stateIndex - 1]?.name}
          </Text>
        )}
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  goBackButton: {
    flexDirection: 'row',
    paddingLeft: 12,
    justifyContent: 'center',
    alignItems: 'center',
  },
  backArrowImage: {
    width: 6,
    height: 10,
    marginRight: 10,
  },
  previousScreenName: {
    fontSize: 14,
    color: theme.colors.turquoise,
  },
});

export default HeaderLeft;
