import React from 'react';
import {
  StyleSheet,
  Text,
  ViewStyle,
  TextStyle,
  View,
  TouchableWithoutFeedback,
  Image,
} from 'react-native';
import { Item } from '../../screens/ListScreen';
import { numberWithCommas } from '../../utils/numberHelper';
import { theme } from '../../theme/globalStyles';

const arrowUpIcon = require('../../assets/arrow-up.png');
const arrowDownIcon = require('../../assets/arrow-down.png');

interface ButtonProps {
  item: Item;
  fullCard?: boolean;
  onPress?: () => void;
}

function ItemCard({ item, fullCard = false, onPress }: ButtonProps) {
  const percent24Hr = Number(item.changePercent24Hr);
  const percentUp = percent24Hr > 0;
  const percent24HrStyle = (
    isUp: ViewStyle | TextStyle,
    isDown: ViewStyle | TextStyle,
  ) => (percentUp ? isUp : isDown);

  return (
    <View style={styles.itemContainer}>
      <TouchableWithoutFeedback onPress={onPress}>
        <View>
          <View style={styles.nameRankRow}>
            <View style={styles.nameSymbolRow}>
              <Text style={styles.itemName}>
                <Text style={styles.itemSymbol}>{item.symbol}</Text> -{' '}
                {item.name}
              </Text>
            </View>
            <Text style={styles.itemRank}>#{item.rank}</Text>
          </View>
          <View style={styles.pricePercent24HrRow}>
            <Text style={styles.itemPrice}>
              $ {numberWithCommas(item.priceUsd)}{' '}
              <Text style={styles.itemPriceCurrency}>USD</Text>
            </Text>
            <View
              style={[
                styles.itemPercent24Hr,
                percent24HrStyle(styles.badgeGreen, styles.badgeRed),
              ]}>
              {percentUp ? (
                <Image source={arrowUpIcon} style={styles.iconArrow} />
              ) : (
                <Image source={arrowDownIcon} style={styles.iconArrow} />
              )}
              <Text
                style={percent24HrStyle(
                  styles.percentageGreen,
                  styles.percentageRed,
                )}>
                {percent24Hr.toFixed(2)}%
              </Text>
            </View>
          </View>
          {fullCard && (
            <View>
              <Text style={styles.detailText}>
                <Text style={styles.detailTitle}>Supply </Text>
                {numberWithCommas(item.supply)}
              </Text>
              <Text style={styles.detailText}>
                <Text style={styles.detailTitle}>Max Supply </Text>
                {numberWithCommas(item.maxSupply)}
              </Text>
              <Text style={styles.detailText}>
                <Text style={styles.detailTitle}>Market Cap </Text>
                {numberWithCommas(item.marketCapUsd)}
              </Text>
            </View>
          )}
        </View>
      </TouchableWithoutFeedback>
    </View>
  );
}

const styles = StyleSheet.create({
  itemContainer: {
    display: 'flex',
    marginHorizontal: 30,
    backgroundColor: theme.colors.white,
    borderRadius: 6,
    elevation: 3,
    marginVertical: 10,
    paddingVertical: 16,
    paddingHorizontal: 14,
    shadowColor: '#171717',
    shadowOffset: {
      width: 1,
      height: 2,
    },
    shadowOpacity: 0.2,
    shadowRadius: 3,
  },
  nameRankRow: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  nameSymbolRow: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  pricePercent24HrRow: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  itemPercent24Hr: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  badgeRed: {
    backgroundColor: theme.colors.badgeRed,
    paddingHorizontal: 10,
    paddingVertical: 3,
    borderRadius: 12,
  },
  badgeGreen: {
    backgroundColor: theme.colors.badgeGreen,
    paddingHorizontal: 10,
    paddingVertical: 3,
    borderRadius: 12,
  },
  percentageGreen: {
    color: theme.colors.green,
    fontSize: 14,
    fontWeight: 'bold',
  },
  percentageRed: {
    fontSize: 14,
    fontWeight: 'bold',
    color: theme.colors.red,
  },
  iconArrow: {
    width: 10,
    height: 12,
    marginRight: 4,
  },
  itemName: {
    color: theme.colors.black,
  },
  itemRank: {
    color: theme.colors.gray,
    fontSize: 14,
    fontWeight: '500',
    marginRight: 10,
  },
  itemSymbol: {
    fontSize: 18,
    fontWeight: 'bold',
  },
  itemPrice: {
    fontSize: 24,
    marginTop: 8,
    fontWeight: 'bold',
    color: theme.colors.turquoise,
  },
  itemPriceCurrency: {
    fontSize: 14,
    fontWeight: '500',
    color: theme.colors.gray,
  },
  detailTitle: {
    fontSize: 16,
    fontWeight: '500',
  },
  detailText: {
    fontSize: 14,
    fontWeight: '400',
    color: theme.colors.black,
    marginTop: 10,
  },
});

export default ItemCard;
