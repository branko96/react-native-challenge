import React from 'react';
import { Image, StyleSheet } from 'react-native';

function Logo() {
  return (
    <Image style={styles.logo} source={require('../../assets/logo.png')} />
  );
}

const styles = StyleSheet.create({
  logo: {
    width: 28,
    height: 26,
  },
});

export default Logo;
