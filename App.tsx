import * as React from 'react';
import { StatusBar } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import ToDoScreen from './screens/ToDoScreen';
import HomeScreen from './screens/HomeScreen';
import ListScreen from './screens/ListScreen';
import DetailScreen from './screens/DetailScreen';
import WalletScreen from './screens/WalletScreen';
import { Logo } from './components/ui';
import { HeaderLeft } from './components/common';
import { theme } from './theme/globalStyles';
import { UserProvider } from './userContext';

export type RootStackParamList = {
  Home: undefined;
  List: undefined;
  ToDo: undefined;
  Wallet: undefined;
  Detail: { itemId: string };
};
const Stack = createStackNavigator<RootStackParamList>();

function App() {
  return (
    <UserProvider>
      <NavigationContainer>
        <Stack.Navigator
          screenOptions={({ navigation }) => ({
            headerTitleAlign: 'center',
            headerTitleStyle: {
              fontSize: 16,
              fontWeight: '600',
              color: theme.colors.black,
            },
            headerStyle: {
              borderBottomWidth: 1,
              borderBottomColor: theme.colors.borderGrey,
            },
            headerLeft: navigation.canGoBack()
              ? () => <HeaderLeft navigation={navigation} />
              : () => null,
          })}>
          <Stack.Screen
            name="Home"
            component={HomeScreen}
            options={{ headerTitle: () => <Logo /> }}
          />
          <Stack.Screen
            name="ToDo"
            component={ToDoScreen}
            options={{ title: 'To Do' }}
          />
          <Stack.Screen name="List" component={ListScreen} />
          <Stack.Screen name="Detail" component={DetailScreen} />
          <Stack.Screen name="Wallet" component={WalletScreen} />
        </Stack.Navigator>
        <StatusBar />
      </NavigationContainer>
    </UserProvider>
  );
}

export default App;
