import EncryptedStorage from 'react-native-encrypted-storage';

export async function storeUserSession(username: string, password: string) {
  try {
    await EncryptedStorage.setItem(
      'user_session',
      JSON.stringify({
        username,
        password,
      }),
    );
  } catch (error) {
    console.log(error);
  }
}

export async function retrieveUserSession() {
  try {
    const session = await EncryptedStorage.getItem('user_session');

    if (session !== undefined && session !== null) {
      return JSON.parse(session);
    }
  } catch (error) {
    console.log(error);
  }
}

export async function removeUserSession() {
  try {
    await EncryptedStorage.removeItem('user_session');
  } catch (error) {
    console.log(error.code);
  }
}
