import React, { useEffect } from 'react';
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  ActivityIndicator,
} from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { ItemCard } from '../components/ui';
import { getAssets } from '../api/assets.service';
import { StackNavigationProp } from '@react-navigation/stack';
import { RootStackParamList } from '../App';
import { theme } from '../theme/globalStyles';

export interface Item {
  id: string;
  rank: string;
  name: string;
  symbol: string;
  priceUsd: string;
  supply: string;
  maxSupply: string | null;
  marketCapUsd: string;
  volumeUsd24Hr: string;
  changePercent24Hr: string;
  vwap24Hr: string;
  explorer: string;
}

type ListScreenProp = StackNavigationProp<RootStackParamList, 'List'>;

export default function ListScreen() {
  const navigation = useNavigation<ListScreenProp>();
  const [items, setItems] = React.useState<Item[]>([]);
  const [loading, setLoading] = React.useState<boolean>(false);
  const onSelectCurrency = (itemId: string) => {
    navigation.navigate('Detail', { itemId });
  };

  useEffect(() => {
    const fetchAssets = () => {
      setLoading(true);
      getAssets().then(assets => {
        setItems(assets);
        setLoading(false);
      });
    };
    fetchAssets();
  }, []);

  return (
    <View style={styles.container}>
      {loading ? (
        <ActivityIndicator size="large" />
      ) : items && items.length > 0 ? (
        <ScrollView>
          {items.map(item => (
            <ItemCard
              key={item.id}
              item={item}
              onPress={() => onSelectCurrency(item.id)}
            />
          ))}
        </ScrollView>
      ) : (
        <View style={theme.alerts.errorAlert}>
          <Text style={theme.alerts.errorAlertTitle}>
            Error fetching assets
          </Text>
        </View>
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 8,
    backgroundColor: theme.backgrounds.greyBackground,
  },
  illustration: {
    width: 50,
    height: 50,
  },
});
