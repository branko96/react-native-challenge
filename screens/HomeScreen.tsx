import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { StackNavigationProp } from '@react-navigation/stack';
import { RootStackParamList } from '../App';
import { UserContextInterface, withUser } from '../userContext';
import { LoginFormValues } from '../components/auth/LoginForm';
import { LoginForm } from '../components/auth';
import { theme } from '../theme/globalStyles';

type HomeScreenProp = StackNavigationProp<RootStackParamList, 'Home'>;

interface HomeScreenProps {
  context: UserContextInterface;
}

const initialValues = {
  username: '',
  password: '',
};

function HomeScreen({ context }: HomeScreenProps) {
  const navigation = useNavigation<HomeScreenProp>();
  const { login } = context;
  const onSignIn = ({ username, password }: LoginFormValues) => {
    login(username, password);
    navigation.navigate('ToDo');
  };

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Welcome</Text>
      <LoginForm initialValues={initialValues} onSubmit={onSignIn} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: theme.colors.white,
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    color: theme.colors.black,
    marginBottom: 34,
  },
});

export default withUser(HomeScreen);
