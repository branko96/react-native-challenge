import { useNavigation } from '@react-navigation/native';
import React from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';
import { Button } from '../components/common';
import { theme } from '../theme/globalStyles';
import { StackNavigationProp } from '@react-navigation/stack';
import { RootStackParamList } from '../App';

type ToDoScreenProp = StackNavigationProp<RootStackParamList, 'ToDo'>;

export default function HomeScreen() {
  const navigation = useNavigation<ToDoScreenProp>();
  const onStart = () => {
    navigation.navigate('List');
  };

  return (
    <View style={styles.container}>
      <Image
        style={styles.illustration}
        source={require('../assets/home-illustration.png')}
      />
      <Text style={styles.title}>Howdy partner!</Text>
      <View style={styles.wrapper}>
        <Text style={styles.textCenter}>This is your assignment.</Text>
        <Text style={styles.textCenter}>
          Follow the instructions on the Readme file.
        </Text>
        <Text style={styles.legend}>
          Don’t worry, it’s easy! You should have the app looking smooth in no
          time.
        </Text>
      </View>
      <View style={styles.buttonContainer}>
        <Button title="Get Started" onPress={onStart} />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: theme.colors.white,
    alignItems: 'center',
    justifyContent: 'center',
  },
  illustration: {
    width: 256,
    height: 256,
  },
  buttonContainer: {
    width: '100%',
    marginTop: 40,
    paddingHorizontal: 30,
  },
  title: {
    fontSize: 24,
    color: theme.colors.black,
    fontWeight: 'bold',
    marginBottom: 12,
  },
  legend: {
    marginTop: 12,
    textAlign: 'center',
    color: theme.colors.black,
  },
  textCenter: {
    textAlign: 'center',
    color: theme.colors.black,
  },
  wrapper: {
    paddingHorizontal: 30,
    marginTop: 12,
  },
});
