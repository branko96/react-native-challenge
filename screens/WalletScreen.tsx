import * as React from 'react';
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  Image,
  TouchableOpacity,
} from 'react-native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { theme } from '../theme/globalStyles';
import { UserContextInterface, withUser } from '../userContext';

const playStoreUrl = 'https://play.google.com/store/apps/details';
const Tab = createBottomTabNavigator();

export default function WalletScreen() {
  return (
    <Tab.Navigator
      screenOptions={({ route, navigation }) => ({
        tabBarIcon: () => null,
        headerShown: false,
        tabBarStyle: {
          height: 60,
        },
        tabBarLabel: ({ focused, color }) => {
          const navigateTo = route.name === 'Account' ? 'Account' : 'Partners';
          return (
            <TouchableOpacity
              style={styles.tabBarLabel}
              onPress={() => navigation.navigate(navigateTo)}>
              <View style={focused ? styles.activeDot : styles.inactiveDot} />
              <Text style={[styles.label, { color }]}>{route.name}</Text>
            </TouchableOpacity>
          );
        },
        tabBarActiveTintColor: theme.colors.turquoise,
        tabBarInactiveTintColor: theme.colors.black,
      })}>
      <Tab.Screen name="Account" component={withUser(AccountSection)} />
      <Tab.Screen name="Partners" component={PartnersSection} />
    </Tab.Navigator>
  );
}

interface AccountSectionProps {
  context: UserContextInterface;
}

function AccountSection({ context }: AccountSectionProps) {
  return (
    <View style={styles.containerWhite}>
      <Image
        style={styles.illustration}
        source={require('../assets/finish-illustration.png')}
      />
      <Text style={styles.title}>Hello, {context?.username}</Text>
      <Text style={styles.legend}>
        Glad you are here, hope to see you soon.
      </Text>
    </View>
  );
}

function PartnersSection() {
  const partnerList = [
    {
      name: 'FacturaPrint',
      url: `${playStoreUrl}?id=com.viasdigitales.facturaprint`,
      comments:
        'Facturation app for your business, connected with AFIP service to get invoices certified for the official governmental organism. Android Java Native.',
    },
    {
      name: 'KW Command',
      url: `${playStoreUrl}?id=com.kw.agent.command`,
      comments:
        'Set of tools to manage your business. This is an internal app for Keller Willams Agents. React Native and Typescript',
    },
    {
      name: 'Wiik',
      url: 'No published app yet',
      comments:
        'App that allows you to register and subscribe to a payment plan that gives you a number of consumitions (beer, coffee, tea, drink) in the added breweries, bars and restaurants. React Native Typescript.',
    },
    {
      name: 'Agora',
      url: 'No published app yet',
      comments:
        'With this app you can register and connect with your neighbors to hire a service of building for the environment, like pavement, parks, green spaces. Connects directly with building companies. React Native Typescript.',
    },
  ];

  interface MyApp {
    name: string;
    url: string;
    comments: string;
  }

  interface ListItemProps {
    item: MyApp;
  }
  const ListItem = ({ item }: ListItemProps) => {
    return (
      <View style={styles.itemContainer}>
        <Text style={styles.itemTitle}>{item.name}</Text>
        <Text style={styles.itemDescription}>{item.comments}</Text>
        <Text style={styles.itemDescription}>
          URL: <Text style={styles.itemUrl}>{item.url}</Text>
        </Text>
      </View>
    );
  };

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Partners</Text>
      <Text style={styles.subtitle}>Some apps I was involved:</Text>
      {partnerList && partnerList.length > 0 ? (
        <ScrollView>
          {partnerList.map(item => (
            <ListItem key={item.name} item={item} />
          ))}
        </ScrollView>
      ) : (
        <Text>No Apps 🙈</Text>
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: theme.backgrounds.greyBackground,
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
  },
  containerWhite: {
    backgroundColor: theme.colors.white,
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    color: theme.colors.black,
    marginBottom: 12,
    marginTop: 14,
  },
  tabBarLabel: {
    width: '70%',
    height: '100%',
  },
  subtitle: {
    color: theme.colors.black,
    fontSize: 16,
    marginBottom: 12,
  },
  legend: {
    fontWeight: '400',
    fontSize: 14,
    color: theme.colors.black,
  },
  illustration: {
    width: 256,
    height: 160,
  },
  itemContainer: {
    display: 'flex',
    marginHorizontal: 30,
    backgroundColor: theme.colors.white,
    borderRadius: 6,
    elevation: 3,
    marginVertical: 6,
    paddingVertical: 16,
    paddingHorizontal: 14,
    shadowColor: '#171717',
    shadowOffset: {
      width: 1,
      height: 2,
    },
    shadowOpacity: 0.2,
    shadowRadius: 3,
  },
  itemTitle: {
    color: theme.colors.turquoise,
    fontWeight: 'bold',
    fontSize: 16,
  },
  itemDescription: {
    color: theme.colors.black,
    fontSize: 14,
    marginTop: 10,
  },
  itemUrl: {
    color: theme.colors.gray,
  },
  activeDot: {
    borderTopWidth: 3,
    height: 3,
    borderTopColor: theme.colors.turquoise,
  },
  inactiveDot: {
    borderTopWidth: 3,
    height: 3,
    borderTopColor: 'white',
  },
  label: {
    textAlign: 'center',
    fontSize: 16,
    fontWeight: 'bold',
    marginTop: 12,
  },
});
