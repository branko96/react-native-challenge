import { useNavigation } from '@react-navigation/native';
import React, { useEffect } from 'react';
import { ActivityIndicator, StyleSheet, Text, View } from 'react-native';
import { Button } from '../components/common';
import { ItemCard } from '../components/ui';
import { Item } from './ListScreen';
import { getAssetById } from '../api/assets.service';
import { theme } from '../theme/globalStyles';
import { StackNavigationProp } from '@react-navigation/stack';
import { RootStackParamList } from '../App';

type DetailScreenProp = StackNavigationProp<RootStackParamList, 'Detail'>;

export default function DetailScreen({ route }: any) {
  const navigation = useNavigation<DetailScreenProp>();
  const { itemId } = route.params;
  const [item, setItem] = React.useState<Item>();
  const [loading, setLoading] = React.useState<boolean>(false);
  const goToWallet = () => {
    navigation.navigate('Wallet');
  };

  useEffect(() => {
    const fetchAsset = () => {
      setLoading(true);
      getAssetById(itemId).then(asset => {
        setItem(asset);
        setLoading(false);
      });
    };
    fetchAsset();
  }, [itemId]);

  return (
    <View style={styles.container}>
      {loading ? (
        <ActivityIndicator size="large" />
      ) : item ? (
        <>
          <ItemCard item={item} fullCard />
          <View style={styles.buttonContainer}>
            <Button title="My Wallet" onPress={goToWallet} />
          </View>
        </>
      ) : (
        <View style={theme.alerts.errorAlert}>
          <Text style={theme.alerts.errorAlertTitle}>Error fetching asset</Text>
        </View>
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: theme.backgrounds.greyBackground,
    paddingTop: 16,
  },
  itemContainer: {
    display: 'flex',
    marginHorizontal: 26,
    backgroundColor: theme.colors.white,
    borderRadius: 6,
    elevation: 3,
    marginVertical: 10,
    paddingVertical: 12,
    paddingHorizontal: 14,
  },
  buttonContainer: {
    marginTop: 16,
    marginHorizontal: 28,
  },
});
