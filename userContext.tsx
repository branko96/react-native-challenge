import React, { createContext, useState } from 'react';
import { storeUserSession } from './utils/secureStoreHelper';

export interface UserContextInterface {
  username: string;
  login: (username: string, password: string) => void;
}

const initialContext: UserContextInterface = {
  username: '',
  login: () => ({}),
};

const UserContext = createContext(initialContext);

interface UserProviderProps {
  children: React.ReactNode;
}

const UserProvider = ({ children }: UserProviderProps) => {
  const [username, setName] = useState('');
  const login = async (name: string, password: string) => {
    setName(name);
    await storeUserSession(name, password);
  };
  return (
    <UserContext.Provider value={{ username, login }}>
      {children}
    </UserContext.Provider>
  );
};

const withUser = (Child: any) => (props: any) =>
  (
    <UserContext.Consumer>
      {(context: UserContextInterface) => (
        <Child {...props} context={context} />
      )}
    </UserContext.Consumer>
  );

export { UserProvider, withUser };
